# Installing gems:
cd /path/to/project/crawler && bundle install --path=.bundle
cd /path/to/project/web-app && bundle install --path=.bundle

# Starting:
cd /path/to/project/crawler && bundle exec ruby main_starter.rb
cd /path/to/project/web-app && bundle exec rails s
