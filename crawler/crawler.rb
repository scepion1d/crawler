require './lib/configs'
require './lib/db_connector'
require './lib/method_detector'
require './lib/logger'
require './lib/file_worker'
FileWorker.require_all('./models/')
require './crawler/src/xml_parser'
require './crawler/src/html_parser'

class Crawler

  def self.xml_is_changed?(current_id)
    old_id = FileWorker.read_from(XML_ID).to_i
     if current_id != old_id
       return old_id
     else
       return current_id
     end
  end


  def self.get_configs_from_xml(tasks)
    __method__ = MethodDetector.get_method_name
    begin
      configs = Array[]
      tasks.each do |task|
        configs << XML_Parser.get_data(task.xml)
      end
      return configs
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.get_configs_from_db
    __method__ = MethodDetector.get_method_name
    begin
      if DB_Connector.connect(DB_HOST)
        return Task.all.to_a
      else
        return Array.new
      end
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.update_xml
    __method__ = MethodDetector.get_method_name
    begin
      if DB_Connector.connect(DB_HOST)
        #Unparsed.create(:xml => '<?xml version="1.0" encoding="UTF-8"?><mask><title>free-lance</title><author>scepion1d</author><link>http://www.free-lance.ru</link><root><tag class="prj-one" id="">div</tag></root><object name="title" target=""><tag class="form" id="">div</tag><tag class="form-in" id="">div</tag><tag class="" id="">h3</tag><tag class="" id="">a</tag></object><object name="link" target="href"><tag class="form" id="">div</tag><tag class="form-in" id="">div</tag><tag class="" id="">h3</tag><tag class="" id="">a</tag></object><object name="descr" target=""><tag class="form" id="">div</tag><tag class="form-in" id="">div</tag><tag class="prj-full-display" id="">div</tag><tag class="utxt" id="">div</tag><tag class="" id="">p</tag></object><object name="price" target=""><tag class="" id="">div</tag><tag class="" id="">div</tag><tag class="" id="">var</tag></object></mask>')
        #Unparsed.create(:xml => '<?xml version="1.0" encoding="UTF-8"?><mask><title>freelance</title><author>scepion1d</author><link>http://freelance.ru</link><root><tag class="proj" id="">div</tag></root><object name="title" target=""><tag class="" id="">h2</tag><tag class="" id="">a</tag><tag class="" id="">span</tag></object><object name="link" target="href"><tag class="" id="">h2</tag><tag class="" id="">a</tag></object><object name="descr" target=""><tag class="" id="">p</tag></object><object name="price" target=""><tag class="" id="">h2</tag><tag class="" id="">span</tag><tag class="" id="">b</tag></object></mask>')
        #Unparsed.create(:xml => '<?xml version="1.0" encoding="UTF-8"?><mask><title>habrahabr</title><author>ailack</author><link>http://habrahabr.ru</link><root><tag class="post" id="">div</tag></root><object name="title" target=""><tag class="title" id="">h1</tag><tag class="post_title" id="">a</tag></object><object name="link" target="href"><tag class="title" id="">h1</tag><tag class="post_title" id="">a</tag></object><object name="blog" target=""><tag class="hubs" id="">div</tag><tag class="hub" id="">a</tag></object><object name="content" target=""><tag class="content" id="">div</tag></object><object name="author" target=""><tag class="infopanel" id="">div</tag><tag class="author" id="">div</tag><tag class="" id="">a</tag></object></mask>')
        #Unparsed.create(:xml => '<?xml version="1.0" encoding="UTF-8"?><mask><title>web-lancer</title><author>scepion1d</author><link>http://web-lancer.ru</link><root><tag class="topic" id="">div</tag></root><object name="title" target=""><tag class="" id="">h1</tag><tag class="" id="">a</tag></object><object name="link" target="href"><tag class="" id="">h1</tag><tag class="" id="">a</tag></object><object name="descr" target=""><tag class="content" id="">div</tag></object></mask>')
        #Unparsed.create(:xml => '<?xml version="1.0" encoding="UTF-8"?><mask><title>lenta.ru/russia</title><author>ailack</author><link>http://lenta.ru/russia/</link><root><tag class="news1" id="">div</tag></root><object name="title" target=""><tag class="" id="">h4</tag><tag class="" id="">a</tag></object><object name="brief" target=""><tag class="" id="">p</tag></object></mask>')
        #Unparsed.create(:xml => '<?xml version="1.0" encoding="UTF-8"?><mask><title>weblancer</title><author>scepion1d</author><link>http://www.weblancer.net</link><root><tag class="items_list" id="">table</tag><tag class="" id="">tr</tag></root><object name="title" target=""><tag class="il_main" id="">td</tag><tag class="item" id="">a</tag></object><object name="link" target="href"><tag class="il_main" id="">td</tag><tag class="item" id="">a</tag></object><object name="price" target=""><tag class="il_medium" id="">td</tag><tag class="" id="">b</tag></object></mask>')
        new_tasks = Array.new
        Unparsed.all.each do |xml|
          task = Task.where(:unparsed_id => xml._id).first
          if task.nil?
            new_tasks << xml
          end
        end
        if new_tasks.count > 0
          configs = self.get_configs_from_xml(new_tasks)
          (0 .. configs.size-1).each do |i|
            user = User.where(:login => configs[i][:author]).first
            unless user.nil?
              Task.new(:user => user, :title => configs[i][:title],:links => configs[i][:links],
                       :root => configs[i][:root], :objects => configs[i][:objects],
                       :categories => Array.new, :unparsed_id => new_tasks[i]._id).save
            end
          end
        end
      end
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
    return true
  end


  def self.processing_configs(configs)
    __method__ = MethodDetector.get_method_name
    begin
      @full_data = Array.new
      parsed_data = nil
      configs.each do |config|
        parsed_data = HTML_Parser.get_data(config)
        @full_data << parsed_data
      end
      return @full_data
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.push_entries_to_db(data, configs)
    __method__ = MethodDetector.get_method_name
    begin
      (0..data.size-1).each do |i|
        new_entries = 0
        data[i].each do |entry|
          params = Hash.new
          params[:user_id] = configs[i].user_id
          params[:task_id] = configs[i]._id
          unless entry.nil?
            entry.each do |key, value|
              params[key.to_sym] = value
            end
          else
            Logger.error_report("Error in #{self.name}.#{__method__}:\n  Nil entry for #{configs[i]._id}")
          end
          if Entry.where(params).first.nil?
            new_entries += 1
            db_entry = Entry.new(params)
            db_entry.time_dim = self.generate_time_dim
            db_entry.category = ""
            db_entry.save
          end
        end

        user = User.where(:_id => configs[i].user_id).first
        user.new_entries = 0 if user.new_entries.nil?
        user.new_entries += new_entries
        user.save
      end
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.generate_time_dim
    __method__ = MethodDetector.get_method_name
    begin
      params = {:day => Time.now.day, :month => Time.now.month,
                :year => Time.now.year, :wday => Time.now.wday,
                :min => Time.now.min, :hour => Time.now.hour}
      return TimeDim.find_or_create_by(params)._id
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end

end
