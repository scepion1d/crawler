require './lib/configs'
require './lib/logger'
require './lib/method_detector'
require './crawler/src/downloader'
require './crawler/src/xml_parser'
require 'nokogiri'

class HTML_Parser

  # Get information blocks from HTML-code
  # @param page [String] HTML-document
  # @param root_tag_path [String] path to block
  # @example
  #     blocks = HTML_Parser.get_blocks(html_document, root_tag_path)
  # @return [Array] array of blocks (each block is a String)
  def self.get_blocks(page, root_tag_path)
    __method__ = MethodDetector.get_method_name
    begin
      html_doc = Nokogiri::HTML(page)
      html_doc.css(root_tag_path).to_a
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # Get information blocks from HTML-code
  # @param page [Arrau] array of html-blocks
  # @example
  #     HTML_Parser.print_blocks(blocks)
  def self.print_blocks(blocks)
    __method__ = MethodDetector.get_method_name
    begin
      (0 .. blocks.size - 1).each do |i|
        puts "#{i}--------------------------------------------------------\n"
        puts "#{blocks[i]}"
        puts "#{i}--------------------------------------------------------\n\n"
      end
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # Parse result blocks for target information
  # @param html_blocks [Array] array of html_blocks
  # @example
  #     blocks = HTML_Parser.get_blocks(html_document, root_tag_path)
  # @return [Array] array of blocks (each block is a String)
  def self.parse(html_blocks, objects)
    __method__ = MethodDetector.get_method_name
    begin
      txt_blocks = Array.[](size = html_blocks.size)

      (0 .. html_blocks.size - 1).each do |i|
        html_block = Nokogiri::HTML(html_blocks[i].to_s)
        txt_block = Hash.new("")

        objects.each { |object|
          #puts object['target']
          if object['target'] != "content"
            info= html_block.css(object['path']).map { |tag| tag["#{object['target']}"] }[0]
          else
            info = html_block.css(object['path'])[0]
            info = info.content.strip if info != nil
          end
          txt_block["#{object['name']}"] = info
        }

        txt_blocks[i] = txt_block
      end

      txt_blocks
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # Delete all whitespaces in html code
  # @param html_code [String] a part of html-code
  # @example translate some text to UTF8
  #     clear_html = HTML_Parser.optimize(html_code)
  # @return [Array] array of blocks (each block is a String)
  def self.optimize(html_code)
    __method__ = MethodDetector.get_method_name
    begin
      html_code.gsub!(/(\n\t)/, ' ')   # remove ends of lines
      html_code.gsub!(/>\s*</, '><')   # remove whitespaces between tags

      html_code.gsub!(/<\s*/, '<')     # remove whitespaces before tags names

      # remove whitespaces before ends of tags
      html_code.gsub!(/\s*\/>/, '/>')
      html_code.gsub!(/\s*>/, '>')

      html_code.gsub!(/\s+/, ' ')      # replace other multiple spaces with single space
      html_code
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # @param url [An url : String]
  # @return [True if url is valid and False if not : Boolean]
  def self.url_is_valid(url)
    return true unless (url =~ URI::regexp).nil?
    false
  end


  def self.print_parsed_data(blocks, file)
    return nil if blocks.nil?

    @file = File.open(file, "w")
    puts "#{file}"
    (0 .. blocks.size - 1).each do |i|
      blocks[i].each { |key, value|
        @file.write("#{key}:   #{value}\n")
      }
      @file.write("\n\n")
    end if blocks.size > 0
  end


  # Parse config file and process data
  # @param config [Task] Mongoid object with config data
  def self.get_data(config)
    __method__ = MethodDetector.get_method_name
    begin
      @parsed_data = Array.new
      config.links.each do |link|
        page = Downloader.get_page(link)
        if page != nil
          utf_page = Downloader.translate_to_utf8(page)
          html_blocks = self.get_blocks(utf_page, config.root)
          data = self.parse(html_blocks, config.objects)
          data.each do |d|
            @parsed_data << d
          end
        end
      end
      @parsed_data
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end

end
