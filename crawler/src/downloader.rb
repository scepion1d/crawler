require './lib/configs'
require './lib/logger'
require './lib/method_detector'
require 'net/http'
require 'open-uri'
require 'ensure'

class Downloader

  # Available encodings
  ENCODINGS = Array['UTF-8', 'WINDOWS-1251', 'KOI-8']

  # @param text [String] Any text
  # @example
  #     utf_text = Downloader.translate_to_utf8(text)
  # @return [String] Text in UTF8 encoding
  def self.translate_to_utf8(text)
    __method__ = MethodDetector.get_method_name
    begin
      encoding = Ensure::Encoding.guess_encoding(text, ENCODINGS).name
      text.ensure_encoding('UTF-8', :external_encoding => encoding)
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # @param address [String] an url
  # @example
  #     html_code = Downloader.get_page('http://example.com/path/to/document.html')
  # @return [String] HTML document from address
  def self.get_page(address)
    response = nil
    __method__ = MethodDetector.get_method_name
    begin
      url = URI.parse(address)
      url.path != "" ? path = url.path : path = "/"
      Net::HTTP.start(url.host, 80) do |http|
        response = http.get(path, "User-Agent" => "#{USER_AGENT_FIREFOX}")
      end
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end

    response.body
  end

end
