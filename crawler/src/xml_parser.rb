require './lib/configs'
require './lib/logger'
require './lib/method_detector'
require 'nokogiri'

class XML_Parser

  # @example
  #     links = Downloader.translate_to_utf8(text)
  # @return [Array] Links from task xml description
  def self.get_links
    __method__ = MethodDetector.get_method_name
    begin
      links = @xml_doc.css("mask link").to_a
      get_tag_content(links)
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # @param tags [Array of xml tags : Array of Nokogiri search results]
  # @return [Content of tags : String]
  def self.get_tag_content(tags)
    __method__ = MethodDetector.get_method_name
    begin
      tags_content = Array.[](size = tags.size)
      (0 .. tags.size - 1).each do |i|
        tags_content[i] = tags[i].content.strip
      end
      tags_content
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # Get path to the root tag in html document
  # @return [Path to root tag : String]
  def self.get_root_tag
    if @xml_doc.nil?
      __method__ = MethodDetector.get_method_name
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end

    tags = @xml_doc.css("mask root tag").to_a
    self.get_path_from(tags)
  end


  def self.get_author
    __method__ = MethodDetector.get_method_name
    begin
      @xml_doc.css("mask author")[0].content.strip
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end

  def self.get_title
    __method__ = MethodDetector.get_method_name
    begin
      @xml_doc.css("mask title")[0].content.strip
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end

  # Get path to target with classes and IDs
  # @param tags [Array of tags from xml document : Array of Nokogiri search results]
  # @return [Path to target : String]
  def self.get_path_from(tags)
    __method__ = MethodDetector.get_method_name
    begin
      path = ""
      (0 .. tags.size - 1).each do |i|
        tag = Nokogiri::XML(tags[i].to_s)
        _class = tag.css("tag").map { |t| t['class'] }[0]
        _id = tag.css("tag").map { |t| t['id'] }[0]
        name = tag.css("tag")[0].content.strip

        path += "#{name}"
        path += ".#{_class}" if _class != ""
        path += "##{_id}" if _id != ""
        path += " " if i < tags.size - 1
      end
      path
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # Get parsing objects from xml document
  # @return [Array of objects : Array[['name' : String, 'target' : String, 'path' : String], ...]]
  def self.get_objects
    __method__ = MethodDetector.get_method_name
    begin
      objects = @xml_doc.xpath("//object").to_a
      result = Array.[](size = objects.size)
      (0 .. objects.size - 1).each do |i|
        obj = Hash.new
        xml_doc = Nokogiri::XML(objects[i].to_s)

        # get object name
        obj[:name] = xml_doc.css("object").map { |o| o['name'] }[0]

        # get object target
        obj[:target] = xml_doc.css("object").map { |o| o['target'] }[0]   # get object target
        obj[:target] = "content" if obj[:target] == ""

        # get path to target
        tags = xml_doc.css("object tag").to_a
        obj[:path] = self.get_path_from(tags)

        result[i] = obj
      end
      result
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # @param file_name [Object]
  def self.get_data(xml)
    __method__ = MethodDetector.get_method_name
    begin
      data = Hash.new(nil)
      @xml_doc = Nokogiri::XML(xml)

      data[:title]   = self.get_title
      data[:author]  = self.get_author
      data[:links]   = self.get_links
      data[:root]    = self.get_root_tag
      data[:objects] = self.get_objects

      self.clear_doc
      data
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  # Remove loaded xml document
  def self.clear_doc
    @xml_doc = nil
  end

end
