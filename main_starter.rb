require './lib/configs'
require './lib/file_worker'
require './crawler/crawler'

print "Updating XML's  "
Crawler.update_xml
print "    done\n"

print "Processing configs  "
configs = Crawler.get_configs_from_db
data = Crawler.processing_configs(configs)
print "    done\n"

print "Pushing data to DB  "
Crawler.push_entries_to_db(data, configs)
print "    done\n"

