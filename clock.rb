require 'clockwork'
include Clockwork

require './lib/configs'
require './lib/file_worker'
require './lib/notifier'
require './crawler/crawler'

every(1.minutes, 'Run crawler') {
  print "Updating XML's  "
  unless Crawler.update_xml.nil?
    print "    done\n"

    print "Processing configs  "
    configs = Crawler.get_configs_from_db
    unless configs.nil?
      data = Crawler.processing_configs(configs)
      unless data.nil?
        print "    done\n"

        print "Pushing data to DB  "
        unless Crawler.push_entries_to_db(data, configs).nil?
          print "    done\n"
        else

          print "    fail\n"
        end
      else

        print "    fail\n"
      end
    end
  else

    print "    fail\n"
  end

  print "Notifying users  "
  DB_Connector.connect(DB_HOST)
  notifier = Notifier.new
  notifier.connect
  puts 'hello'
  User.where(:new_entries.gt => 0).to_a.each do |user|
    notifier.publish_number user.new_entries, user.login
  end

}
