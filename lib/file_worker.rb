require 'digest/sha2'
require './lib/logger'
require './lib/method_detector'

class FileWorker

  def self.read_from(path)
    __method__ = MethodDetector.get_method_name
    begin
      file = File.open(path, "rb")
      file.read.to_s
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.write_to(file, message)
    __method__ = MethodDetector.get_method_name
    begin
      File.open(file, "w") {|f| f.write("#{message}")}
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.append_to(file, message)
    __method__ = MethodDetector.get_method_name
    begin
      File.open(file, "a") {|f| f.write("#{message}")}
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.report_to(log, message)
    __method__ = MethodDetector.get_method_name
    begin
      self.append_to(log, "#{Time.new.getlocal}\n#{message}\n\n")
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.require_all(path)
    __method__ = MethodDetector.get_method_name
    begin
      Dir[File.join(path, "*.rb")].each do |f|
        require f
      end
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.get_dir_checksum(path)
    __method__ = MethodDetector.get_method_name
    begin
      files = Dir["#{path}/**/*"].reject{|f| File.directory?(f)}
      content = files.map{|f| File.read(f)}.join
      checksum = Digest::SHA2.new << content
      return checksum .to_s
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end


  def self.get_files_list(path)
    __method__ = MethodDetector.get_method_name
    begin
      files = Array[]
      Dir.foreach(path) do |file|
        if file == file.scan(/.+\.xml?/i)[0]
          files << file
        end
      end
      return files
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end

end