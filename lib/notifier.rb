require './lib/configs'
require './lib/pubnub'

class Notifier

  def connect
    @pubnub = Pubnub.new( PUBLISH_KEY, SUBSCRIBE_KEY, SECRET_KEY, CIPHER_KEY, SSL_ON )
  end

  def publish_number(number, to)
    info = @pubnub.publish({
               'channel' => to,
               'message' => number
    })

    puts "#{info[1]} to #{to}"
  end

end
