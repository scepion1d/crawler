require './lib/configs'
require './lib/logger'
require './lib/method_detector'
require 'uri'
require 'mongoid'

class DB_Connector

  # @param db_name [String]
  def self.connect(host)
    __method__ = MethodDetector.get_method_name
    begin
      Mongoid.configure do |config|
        conn = Mongo::Connection.from_uri(host)
        db = conn.db(DB_NAME)
        config.master = db
        config.persist_in_safe_mode = false
      end
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.#{__method__}:\n  #{e.message}\n  #{e.backtrace}")
      return false
    end
    return true
  end

end