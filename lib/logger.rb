require './lib/configs'
require './lib/file_worker'

class Logger
  def self.error_report(message)
    FileWorker.report_to(ERROR_LOG, message)
  end
end