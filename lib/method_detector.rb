require "./lib/logger"

module MethodDetector
  def self.get_method_name
    begin
      caller[0]=~/`(.*?)'/
      $1
    rescue Exception => e
      Logger.error_report("Error in #{self.name}.get_method_name:\n  #{e.message}\n  #{e.backtrace}")
      return nil
    end
  end
end