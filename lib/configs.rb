APPLICATION_DIR = File.absolute_path('.')

# XML
XML_DIR = APPLICATION_DIR + '/xml/enabled'

XML_ID = APPLICATION_DIR + "/lib/xml_id"

# FireFox User-Agent
USER_AGENT_FIREFOX = "(\"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5\")"

# Source DB preferences
DB_NAME = 'dana'
DB_USER = 'crawler'
DB_PASS = 'extensapowerbutton'
DB_HOST = "mongodb://#{DB_USER}:#{DB_PASS}@ds033477.mongolab.com:33477/#{DB_NAME}"
#DB_HOST = "mongodb://localhost:27017/dana-x"

# logs preferences
LOGS_DIR = APPLICATION_DIR + "/logs"
ERROR_LOG = "#{LOGS_DIR}/error_log"


# PubNub data
PUBLISH_KEY   = 'pub-2ac280d8-894c-47d1-90db-a94f40bd55e5'
SUBSCRIBE_KEY = 'sub-d1e3ec0c-aa49-11e1-9ee9-058c2234feb4'
SECRET_KEY    = 'sec-N2FmZWRlMWQtY2JhYS00YzgzLThmZGUtNjkxYmE5OWViMTRm'
CIPHER_KEY    = ''
SSL_ON        = false