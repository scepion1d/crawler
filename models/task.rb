require 'mongoid'

class Task
  include Mongoid::Document

  belongs_to :user
  belongs_to :unparsed

  field :title, :type => String
  field :links, :type => Array
  field :root, :type => String
  field :objects, :type => Array
  field :categories, :type => Array

  has_many :entries

end
