require 'mongoid'

class Entry
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  belongs_to :user
  belongs_to :task
  belongs_to :time_dim

  field :category, :type => String

  # any other dynamic fields
end
