require 'mongoid'

class User
  include Mongoid::Document

  field :token, :type => String

  field :login, :type => String
  field :password, :type => String
  field :email, :type => String
  field :new_entries, :type => Integer

  has_many :tasks

  before_create :generate_token
  before_create :clear_new_entries

  def generate_token(length = 10)
    begin
      @new_token = SecureRandom.base64(length).gsub("/","_").gsub(/=+$/,"")
    end while !User.where(:token => @new_token).first.nil?
    self.token = @new_token
  end

end
