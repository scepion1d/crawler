require 'mongoid'

class TimeDim
  include Mongoid::Document

  field :day, :type => Integer
  field :month, :type => Integer
  field :year, :type => Integer
  field :wday, :type => Integer
  field :min, :type => Integer
  field :hour, :type => Integer

  has_many :entries

end