require 'mongoid'

class Unparsed
  include Mongoid::Document

  field :xml, :type => String

  has_one :task
end